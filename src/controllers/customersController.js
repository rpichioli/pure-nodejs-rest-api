const customers = require('../data/database.json'); // Emulating database
const helpers = require('../utils/helpers');

class customersController {
	/** GET /customers */
	async index (req, res) {
		try {
			return helpers.success(res, customers);
    } catch (error) {
      return helpers.error(res, error);
		}
	}

	/** GET /customers/:id */
  async show (req, res, param) {
		try {
      const customer = await customers.filter(x => x.id === param.id)[0];
      return helpers.success(res, customer);
	  } catch (error) {
      return helpers.error(res, error);
	  }
	}

	/** POST /customers/ */
	async new (req, res, param, body) {
		try {
			if (!body) throw 'Body have not been sent!'; // Some basic validation
			const { name, country } = body;
			const collection = [...customers, { _id: helpers.nextSerial(customers), name, country }];
			const error = await helpers.writeFile('./data/database.json', JSON.stringify(collection));
			if (error) throw error;
			return helpers.success(res, collection);
		} catch (error) {
			return helpers.error(res, error);
		}
	}

	/** PUT /customers */
	async edit (req, res, param, body) {
		try {
			if (!body) throw 'Body have not been sent!'; // Some basic validation
			const { _id, name, country } = body;
			const position = customers.findIndex(x => x._id === _id);
			if (!position && position !== 0) throw 'Invalid record has been passed, verify your request body.';
			let collection = customers;
			collection[position] = { _id, name, country };
			const error = await helpers.writeFile('./data/database.json', JSON.stringify(collection));
			if (error) throw error;
			return helpers.success(res, collection);
		} catch (error) {
			return helpers.error(res, error);
		}
	}

	/** DELETE /customers/:id */
	async remove (req, res, param, body) {
		try {
			const position = customers.findIndex(x => x._id === parseInt(param));
			if (position < 0) throw 'Invalid record has been passed, verify your request body.';
			const collection = customers.splice(position, 1); // Array of removed items
			const error = await helpers.writeFile('./data/database.json', JSON.stringify(customers));
			if (error) throw error;
			return helpers.success(res, customers);
		} catch (error) {
			return helpers.error(res, error);
		}
	}
}

module.exports = new customersController();
