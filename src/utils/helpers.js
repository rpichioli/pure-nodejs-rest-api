// Dependencies ------------------------------------------------------------
const EventEmitter = require('events');
const fs = require('fs');

// Event Emmiter ------------------------------------------------------------
const ee = new EventEmitter();
ee.on('log', (...args) => console.log(`${new Date().toLocaleDateString('pt-br')} ${new Date().toLocaleTimeString('pt-br')} --`, args.join(', ')));

// Set Headers -------------------------------------------------------------
const addHeaders = (res, statusCode) => res.writeHead(statusCode, { 'Content-Type': 'application/json' });

/**
 * Return erro to the requester with it's respective headers, proper status and data
 * @param  {Object} res              Response received from server routing control
 * @param  {String} [error='Unknown error occurred'] Error message (optional)
 * @param  {Number} [statusCode=500] Status code (optional)
 */
module.exports.error = (res, error = 'Unknown error occurred', statusCode = 500) => {
	ee.emit('log', 'Entered error helper', error);
	addHeaders(res, statusCode);
	res.end(JSON.stringify({ status: 'fail', error }, null, 3));
}

/**
 * Return success to the requester with it's respective headers and data
 * @param  {Object} res         Response received from server routing control
 * @param  {Object} [data=null] Data (optional with null by default)
 */
module.exports.success = (res, data = null) => {
	addHeaders(res, 200);
	res.end(JSON.stringify({ status: 'success', data }, null, 3));
}

/**
 * Trigger 'log' Event Emmiter
 * @param  {array} args Rest operator
 */
module.exports.log = (...args) => ee.emit('log', args);

/**
 * Return next serial based in a collection
 * @param  {Array}  [collection=[]] Collection to identify max ID
 * @return {Number}                 Next serial calculated
 */
module.exports.nextSerial = (collection = []) => (Math.max.apply(Math, collection.map(item => item._id)) || 0) + 1;

/**
 * Writes content in file through Node's filesystem
 * @param  {String} path             Path to file
 * @param  {String} content          Content that will be write in file
 * @param  {String} [charset='utf8'] Charset to encode file's content
 */
module.exports.writeFile = (path, content, charset = 'utf8') => fs.writeFile(path, content, charset, error => error);
