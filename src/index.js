// Load Environment Variables (.env) ----------------------------------------
require('dotenv').config();

// Dependencies -------------------------------------------------------------
const http = require('http');
const helpers = require('./utils/helpers');

// Safely exceptions handling -----------------------------------------------
process.on('uncaughtException', err => helpers.log('Uncaught exception has been detected', err));
process.on('unhandledRejection', err => helpers.log('Unhandled exception has been detected', err));

// Server -------------------------------------------------------------------
const server = http.createServer(async (req, res) => {
	await require('./core/handler')(req, res, require('./core/routes')); // Orchestrates server job
});
server.listen(process.env.PORT, () => helpers.log(`Localhost listening to port ${process.env.PORT}`));
